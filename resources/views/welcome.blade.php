
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Enzimer</title>
  <meta content="" name="description">
  <meta content="" name="keywords">
  <!-- Favicons -->
  <link href="{{asset('assets/img/favicon.png')}}" rel="icon">
  <link href="{{asset('assets/img/apple-touch-icon.png')}}" rel="apple-touch-icon">
  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
  <!-- Vendor CSS Files -->
  <link href="{{asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('assets/vendor/icofont/icofont.min.css')}}" rel="stylesheet">
  <link href="{{asset('assets/vendor/boxicons/css/boxicons.min.css')}}" rel="stylesheet">
  <link href="{{asset('assets/vendor/venobox/venobox.css')}}" rel="stylesheet">
  <link href="{{asset('assets/vendor/remixicon/remixicon.css')}}" rel="stylesheet">
  <link href="{{asset('assets/vendor/owl.carousel/assets/owl.carousel.min.css')}}" rel="stylesheet">
  <link href="{{asset('assets/vendor/aos/aos.css')}}" rel="stylesheet">
  <!-- Template Main CSS File -->
  <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">
</head>

<body>
      <!-- ======= Header ======= -->
      <header id="header" class="fixed-top d-flex align-items-center header-transparent">
        <div class="container d-flex align-items-center">
          <div class="logo mr-auto">
            <h1 class="text-light"><a href="index.html"><img src="assets/img/logo-blanco.png" alt="" class="img-fluid rounded-lg shadow-md"> <span>Enzimer</span></a></h1>
          </div>
          <nav class="nav-menu d-none d-lg-block">
            <ul>
              <li class="active"><a href="#hero">Home</a></li>
              <li><a href="#details">Pilares</a></li>
              <li><a href="#contact">Contacto</a></li>
              <li>
                <a href="#">
                  <i class='bx bx-user-circle' style="font-size: 25px; color: #1acc8d"></i> 
                  <span style="position: absolute">Login</span></a></li>
    
            </ul>
          </nav><!-- .nav-menu -->
    
        </div>
      </header><!-- End Header -->
    
      <!-- ======= Hero Section ======= -->
      <section id="hero">
        <div class="container">
          <div class="row">
            <div class="col-lg-7 pt-5 pt-lg-0 order-2 order-lg-1 d-flex align-items-center">
              <div data-aos="zoom-out">
                <h1>Subir tu puntaje nunca habia sido tan <span>fácil</span></h1>
                <h2>Con <span class="font-weight-bold text-white">Enzimer</span>, podrás complementar tus clases, reteniendo más y acelerando tu aprendizaje.</h2>
                <div class="text-center text-lg-left">
                  <a href="" class="btn-get-started scrollto text-uppercase">prueba gratis!</a>
                  <a data-toggle="modal" data-target="#exampleModal" class="scrollto text-capitalize ml-4 btn-link btn text-white"><i class='bx bx-right-arrow-alt'></i> Mira como funciona</a>
                </div>
              </div>
            </div>
            <div class="col-lg-5 order-1 order-lg-2 hero-img" data-aos="zoom-out" data-aos-delay="300">
              <img src="assets/img/hero-img.png" class="img-fluid rounded-lg shadow-md animated" alt="">
            </div>
          </div>
        </div>
    
        <svg class="hero-waves" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 24 150 28 " preserveAspectRatio="none">
          <defs>
            <path id="wave-path" d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z">
          </defs>
          <g class="wave1">
            <use xlink:href="#wave-path" x="50" y="3" fill="rgba(255,255,255, .1)">
          </g>
          <g class="wave2">
            <use xlink:href="#wave-path" x="50" y="0" fill="rgba(255,255,255, .2)">
          </g>
          <g class="wave3">
            <use xlink:href="#wave-path" x="50" y="9" fill="#fff">
          </g>
        </svg>
    
      </section><!-- End Hero -->
    
      <main id="main">
        <!-- ======= About Section ======= -->
        <section id="about" class="about bg-light my-5">
          <div class="container">
    
            <div class="row">
                <div class="col-12 text-center icon-boxes py-5">
                    <h3 data-aos="fade-left">Perfecto para todo estudiante, sin importar el punto de partida</h3>
                    <h4 data-aos="fade-left">Enzimer es la mejor herramienta para complementar tus estudios preuniversitarios</h4>
                    <div class="row">
                        <div class="col-md-6 icon-box text-justify" data-aos="fade-left">
                            <div class="icon"><i class="bx bx-check"></i></div>
                            <h4 class="title">Complemento perfecto si estás en academia</h4>
                            <p class="description">Podrás recordar los primeros temas a medida que avances, con repasos eficientes y estratégicos.</p>
                        </div>
                        <div class="col-md-6 icon-box text-justify" data-aos="fade-left">
                            <div class="icon"><i class="bx bx-check"></i></div>
                            <h4 class="title">Potencia tu capacidad de memorización </h4>
                            <p class="description">Sigues olvidando ese presidente o inca? Ya no más con nuestra herramienta de retención dinámica                        </p>
                        </div>
                        <div class="col-md-6 icon-box text-justify" data-aos="fade-left">
                            <div class="icon"><i class="bx bx-check"></i></div>
                            <h4 class="title">Olvidate de Anki y de hacer flashcards</h4>
                            <p class="description">Todos los conceptos de alta rentabilidad los tendrás listos para repasar y memorizar con la repetición espaciada</p>
                        </div>
                        <div class="col-md-6 icon-box text-justify" data-aos="fade-left">
                            <div class="icon"><i class="bx bx-check"></i></div>
                            <h4 class="title">Más aprendizaje, menos preocupaciones</h4>
                            <p class="description">El "eso no viene" no será un problema. Hemos cubierto todas las fijas en flashcards y preguntas.</p>
                        </div>
                    </div>
                </div>
            </div>
    
          </div>
        </section><!-- End About Section -->
        <!-- ======= Details Section ======= -->
        <section id="details" class="details">
            <div class="container">
                <div class="row content">
                    <div class="col-md-4 my-auto" data-aos="fade-right">
                        <img src="assets/img/details-1.png" class="img-fluid rounded-lg shadow-md shadow" alt="">
                    </div>
                    <div class="col-md-8 pt-4" data-aos="fade-up">
                        <h3>Banqueo selecto.</h3>
                        <p>
                            Enzimer cuenta con un amplio banco de preguntas, todas divididas por curso, tema, y dificultad <br>
                        </p>
                        <p>                
                            Puedes subrayar los datos en la misma pregunta, para destacar lo fundamental. <br>
                        </p>
                        <p>
                            Quiere ir practicando antes de clase? o tal vez reforzar lo aprendido? Nuestra web te brinda todo
                        </p>
                        <ul>
                            <li><i class="icofont-check"></i> + 10 00 preguntas.</li>
                            <li><i class="icofont-check"></i> Practice Testing.</li>
                            <li><i class="icofont-check"></i> Dificultad facil / intermedio / DECOe.</li>
                        </ul>
                    </div>
                </div>
                <div class="row content">
                    <div class="col-md-4 order-1 my-auto order-md-2" data-aos="fade-left">
                    <img src="assets/img/details-2.png" class="img-fluid rounded-lg shadow-md shadow" alt="">
                    </div>
                    <div class="col-md-8 pt-5 order-2 order-md-1" data-aos="fade-up">
                        <h3>Flashcards</h3>
                        <p>
                            Enzimer acelera la memoria con flashcards. Estas pueden ser resueltas en repasos de 10 - 15 minutos al día. Contamos con el algoritmo de repetición espaciada con la que los conceptos se quedarán en tu memoria a largo plazo.
                        </p>
                        <ul>
                            <li><i class="icofont-check"></i> + 3 000 flashcards.</li>
                            <li><i class="icofont-check"></i> Repetición espaciada.</li>
                            <li><i class="icofont-check"></i> Active Recall.</li>
                        </ul>
                    </div>
                </div>
                <div class="row content">
                    <div class="col-md-4 my-auto" data-aos="fade-right">
                    <img src="assets/img/details-3.png" class="img-fluid rounded-lg shadow-md shadow mt-5" alt="">
                    </div>
                    <div class="col-md-8 pt-5" data-aos="fade-up">
                        <h3>Horario inteligente</h3>
                        <p>Nuestro horario permite que puedas estudiar progresivamente cada curso, te sugerirán temas (sesiones de preguntas y flashcards).</p>
                        <p>Tendrás repasos intercalados y la dificultad irá incrementando progresivamente creando el efecto de práctica deliberada.</p>
                        <ul>
                            <li><i class="icofont-check"></i> Programa tus flashcards.</li>
                            <li><i class="icofont-check"></i> Repasos intercalados.</li>
                            <li><i class="icofont-check"></i> Interleaving.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section><!-- End Details Section -->
        <!-- ======= Features Section ======= -->
        <section id="features" class="about section-bg">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center icon-boxes py-5">
                        <h3 data-aos="fade-left">Científicamente probado</h3>
                        <p data-aos="fade-left">Enzimer se basa en estrategias de estudio basadas en la evidencia, integradas en una sola herramienta.</p>
                        <div class="row px-5  mt-5">
                            <div class="col-md-6 px-5 text-justify icon-box" data-aos="fade-left">
                                <div class="icon"><i class="bx bx-brain"></i></div>
                                <h5 class="title">Space Repetition</h5>
                                <p class="description">Toma ventaja del "efecto espaciado" para memorizar más rápido.</p>
                            </div>
                            <div class="col-md-6 px-5 text-justify icon-box" data-aos="fade-left">
                                <div class="icon"><i class="bx bx-border-outer"></i></div>
                                <h5 class="title">Interleaving</h5>
                                <p class="description">Ordenamiento estratégico de los temas para su mejor comprensión y consolidación.</p>
                            </div>
                            <div class="col-md-6 px-5 text-justify icon-box" data-aos="fade-left">
                                <div class="icon"><i class="bx bx-test-tube"></i></div>
                                <h5 class="title">Active Recall</h5>
                                <p class="description">El método de estudio más eficiente, sacandole jugo al "efecto testing".</p>
                            </div>
                            <div class="col-md-6 px-5 text-justify icon-box" data-aos="fade-left">
                                <div class="icon"><i class="bx bx-trending-up"></i></div>
                                <h5 class="title">Más aprendizaje, menos preocupaciones</h5>
                                <p class="description">El nivel de desafio optimo acelera el aprendizaje. Ni tan fácil ni tan dificil​.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- End Features Section -->
        <!-- ======= F.A.Q Section ======= -->
        <section id="faq" class="faq">
          <div class="container">
    
            <div class="section-title" data-aos="fade-up">
              <h2>F.A.Q</h2>
              <p>Preguntas frecuentes</p>
            </div>
    
            <div class="faq-list">
              <ul>
                <li class=" shadow-sm" data-aos="fade-up">
                  <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" class="collapse" href="#faq-list-1">
                    ¿Enzimer es útil si aún estoy en el colegio?
                  <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                  <div id="faq-list-1" class="collapse show" data-parent=".faq-list">
                    <p>
                      Claro que sí, Enzimer está diseñado para que puedas reforzar tus clases con sesiones de repaso rápidas (flashcards) y con preguntas de exámenes de admisión (banqueo)
                      Puedes organizarlas a tu gusto y a tu ritmo. Enzimer será tu complemento perfecto.                      
                    </p>
                  </div>
                <li class=" shadow-sm" data-aos="fade-up">
                  <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" class="collapse" href="#faq-list-2">
                    ¿Cómo Enzimer funciona de complemento a una academia?
                  <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                  <div id="faq-list-2" class="collapse" data-parent=".faq-list">
                    <p>
                      El clásico estudiante lo que hace es escuchar videoclases, anotar en el cuaderno los ejercicios y hacer apuntes para luego releerlos. Todos estos métodos de estudio son ineficientes, y es ahí donde Enzimer complementa cualquier academia, libro, o rutina de estudio.
                      Enzimer le otorga al estudiante un portal web/mobile donde puede aplicar todo lo aprendido en sus clases, usando técnicas de estudio con fundamento científico como la Recuerda Activa y la Repetición Espaciada. 
                      Los estudiantes que repasan sus clases con Enzimer, lo hacen mucho más rápido y aseguran que la información aprendida se mantiene a largo plazo, a comparación de releer apuntes o escuchar de nuevo clases.
                      Además, gracias al horario inteligente, tú puedes programar y reprogramar clases para que la misma aplicación te haga recordar cuando te toca repasar un tema. Enzimer le da la flexibilidad que necesitas a tu preparación.               
                    </p>
                  </div>
                </li>
                <li class=" shadow-sm" data-aos="fade-up">
                  <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" class="collapse" href="#faq-list-3">
                    ¿Cómo puedo repasar un tema en solo 10-15 minutos con Enzimer?
                  <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                  <div id="faq-list-3" class="collapse" data-parent=".faq-list">
                    <p>
                      Uno de los pilares de Enzimer son las Flashcards, estas son conceptos puntuales y rentables, los cuales la aplicación te los preguntará de forma rápida y sencilla. Tenemos flashcards de todos los temas que vienen en los distintos exámenes de admisión. Estan ordenadas por curso y clase, así que, si en la mañana tuviste una clase de Bioelementos, con Enzimer puedes repasarla después en tan solo 10-15 minutos con estas tarjetas de revisión rápida (flashcards). 
                      Y no solo eso, gracias al algoritmo de la repetición espaciada, la aplicación te las volverá a preguntar dentro de 1, 5, 14 días, etc. Esto con la intención de vencer a la curva del olvido (todos nos olvidamos lo que aprendemos con el tiempo), y puedas retener todas las fijas hasta llegar a tu examen de admisión!
                      No recomendamos el uso de otro software de repetición espaciada ya que esto generaría redundancia innecesaria de conceptos. Enzimer funciona de manera eficaz junto a cualquier videoclase de cualquier ciclo de tu academia o colegio, así como cualquier material de lectura o banqueo.
                    </p>
                  </div>
                </li>
                <li class=" shadow-sm" data-aos="fade-up">
                  <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" class="collapse" href="#faq-list-4">
                    ¿Cada cuanto tiempo debería estudiar en la aplicación?
                  <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                  <div id="faq-list-4" class="collapse" data-parent=".faq-list">
                    <p>
                      Enzimer está diseñado para que puedas estudiar a tu ritmo, usualmente los estudiantes avanzan 2- 3 temas al día durante su preparación preuniversitaria. Lo que logra Enzimer es que estos repasos sean mucho más rápidos y eficientes. 
                      Un estudiante promedio se pasa 2 a 3 horas repasando 1 tema. 
                      En cambio, con esta aplicación, por tema te sugerimos repasarlo con flashcards (10-15 minutos), más una sesión de banqueo con 12 a 15 preguntas tipo admisión (20-30 minutos). 
                      La misma aplicación tiene más de 10 000 preguntas comentadas tipo examen de admisión, así como más de 4 000 flashcards comentadas. 
                    </p>
                  </div>
                </li>
                <li class=" shadow-sm" data-aos="fade-up">
                  <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" class="collapse" href="#faq-list-4">
                    ¿Cómo estudio con Enzimer si soy autodicacta?
                  <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                  <div id="faq-list-4" class="collapse" data-parent=".faq-list">
                    <p>
                      Hay muchos estudiantes que deciden prepararse por su cuenta (sin una academia) ya que hay una variedad de videos en Youtube de casi todo el temario de San Marcos. Sin embargo, el gran problema que tienen es la organización. Enzimer soluciona esto con el horario inteligente. En el cual puedes programar tu ”ciclo autodidacta” desde el día que quieras iniciar a estudiar hasta el día de tu examen de admisión. La misma aplicación te sugerirá temas y te olvidarás del “qué me toca estudiar hoy?”.
                      Además, no tendrás que gastar en más material o libros. Enzimer tiene un banco de preguntas enorme, todas estas comentadas, organizadas por curso – tema y dificultad. Esta aplicación es la alternativa más económica que tendrás en toda tu preparación.
                    </p>
                  </div>
                </li>
              </ul>
            </div>
    
          </div>
        </section><!-- End F.A.Q Section -->
    
        <!-- ======= Contact Section ======= -->
        <section id="contact" class="contact " style="background-color: #181b93">
          <div class="container">
    
            <div class="section-title text-white" data-aos="fade-up">
              <h2 class="text-white">Contacto</h2>
              <p class="text-light">Póngase en contacto con nosotros</p>
            </div>
    
            <div class="row ">
    
              <div class="col-lg-4" data-aos="fade-right" data-aos-delay="100">
                <div class="info p-4">
                  <div class="address">
                    <i class="icofont-google-map"></i>
                    <h4>Dirección</h4>
                    <p><a target="_blank" href="https://www.google.com/maps/place/Loma+de+los+Suspiros+415,+Lima+15039,+Perú/@-12.1392128,-76.9878836,17z/data=!3m1!4b1!4m5!3m4!1s0x9105b8729e52406b:0xf27f6d3c5a30e239!8m2!3d-12.1392128!4d-76.9856896?hl=es">Loma de los suspiros 415, Santiago de Surco​</a></p>
                  </div>
    
                  <div class="email">
                    <i class="icofont-envelope"></i>
                    <h4>Email</h4>
                    <p><a href="mailto:enzimerperu@gmail.com">enzimerperu@gmail.com</a></p>
                  </div>
    
                  <div class="phone">
                    <i class="icofont-phone"></i>
                    <h4>Teléfono</h4>
                    <p><a href="https://wa.me/936280102" target="_blank">936280102</a></p>
                  </div>
    
                </div>
    
              </div>
    
              <div class="col-lg-8 mt-5 mt-lg-0 bg-section" data-aos="fade-left" data-aos-delay="200">
    
                <form action="forms/contact.php" method="post" role="form" class="bg-section">
                  <div class="form-row">
                    <div class="col-md-6 form-group">
                      <input type="text" name="name" class="form-control" id="name" placeholder="Su nombre" data-rule="minlen:4" data-msg="Ingrese por lo menos 4 caracteres" />
                      <div class="validate"></div>
                    </div>
                    <div class="col-md-6 form-group">
                      <input type="email" class="form-control" name="email" id="email" placeholder="Su email" data-rule="email" data-msg="Por favor coloque una direccion de correo" />
                      <div class="validate"></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <input type="phone" class="form-control" name="phone" id="phone" placeholder="Su n° de telefono" data-rule="minlen:4" data-msg="Ingrese un numero de telefono" />
                    <div class="validate"></div>
                  </div>
                  <div class="form-group">
                    <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Indiquenos su pregunta" placeholder="Mensaje"></textarea>
                    <div class="validate"></div>
                  </div>
                  <div class="text-center"><button type="submit" class="btn btn-success" style="border-radius: 30px">Enviar Mensaje</button></div>
                </form>
    
              </div>
    
            </div>
    
          </div>
        </section><!-- End Contact Section -->
      </main><!-- End #main -->
    <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>
    <div id="preloader"></div>
  @include('partials.video')
  <!-- Vendor JS Files -->
  <script src="{{('assets/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{('assets/vendor/jquery.easing/jquery.easing.min.js')}}"></script>
  <script src="{{('assets/vendor/php-email-form/validate.js')}}"></script>
  <script src="{{('assets/vendor/venobox/venobox.min.js')}}"></script>
  <script src="{{('assets/vendor/waypoints/jquery.waypoints.min.js')}}"></script>
  <script src="{{('assets/vendor/counterup/counterup.min.js')}}"></script>
  <script src="{{('assets/vendor/owl.carousel/owl.carousel.min.js')}}"></script>
  <script src="{{('assets/vendor/aos/aos.js')}}"></script>

  <!-- Template Main JS File -->
  <script src="{{('assets/js/main.js')}}"></script>

</body>

</html>